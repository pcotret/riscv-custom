.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Adding custom instructions in the RISC-V ISA
============================================

The RISC-V instruction set is known to be a modular ISA :cite:p:`riscv_isa`: it is usually composed of a common base (RVI, for integers manipulation) and additional extensions (multiplications, compressed instructions, floating numbers...). It also accepts "external" extensions (cryptography :cite:p:`riscv_crypto`, cache handling :cite:p:`riscv_cache` or even JIT-related operations :cite:p:`riscv_jit`). This tutorial shows how to add a simple instruction to the RVI subset: as a consequence, it will consist of modifications of existing files that are included in all RISC-V ISA combinations (RVI is included by default).

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   adding_custom
   sw_toolchain

References
==========
.. bibliography::