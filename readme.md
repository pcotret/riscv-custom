https://pcotret.gitlab.io/riscv-custom/ for the online version

To generate the documentation

```bash
sudo apt install python3-pip python3-sphinx
pip3 install -r requirements.txt
make [html|latexpdf]
```