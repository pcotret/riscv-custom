Custom instruction in an hardware implementation - picorv32 use case
====================================================================

Study of the processor
----------------------

The source code is available at: https://github.com/YosysHQ/picorv32. Default simulation can be run with :code:`make test` at the repository root:

.. code-block:: console

    $ make test        
    [...]
    /opt/riscv32imc/bin/riscv32-unknown-elf-gcc -c -mabi=ilp32 -march=rv32im -o tests/add.o -DTEST_FUNC_NAME=add \
    -DTEST_FUNC_TXT='"add"' -DTEST_FUNC_RET=add_ret tests/add.S
    [...]
    add..OK
    [...]
    ALL TESTS PASSED. 

A synthesis report is easily obtained:

.. code-block:: console

    $ cd scripts/fpga
    $ make area
    [...]
    +----------------------------+------+-------+-----------+-------+
    |          Site Type         | Used | Fixed | Available | Util% |
    +----------------------------+------+-------+-----------+-------+
    | Slice LUTs*                |  922 |     0 |     41000 |  2.25 |
    |   LUT as Logic             |  874 |     0 |     41000 |  2.13 |
    |   LUT as Memory            |   48 |     0 |     13400 |  0.36 |
    |     LUT as Distributed RAM |   48 |     0 |           |       |
    |     LUT as Shift Register  |    0 |     0 |           |       |
    | Slice Registers            |  583 |     0 |     82000 |  0.71 |
    |   Register as Flip Flop    |  583 |     0 |     82000 |  0.71 |
    |   Register as Latch        |    0 |     0 |     82000 |  0.00 |
    | F7 Muxes                   |    0 |     0 |     20500 |  0.00 |
    | F8 Muxes                   |    0 |     0 |     10250 |  0.00 |
    +----------------------------+------+-------+-----------+-------+

.. _picorv32:
.. figure:: img/picorv32.png
    :align: center

    :code:`picorv32` entity

Behavior of a new instruction
-----------------------------

todo

Adding a coprocessor for the picorv32
-------------------------------------

todo

