Adding a custom instruction in the RVI subset
=============================================

Custom opcodes
--------------
In the existing instruction encoding, there are some opcodes available for custom instructions as shown in :numref:`tab-opcode`.

.. _tab-opcode:
.. figure:: tables/base_opcode.png
    :align: center

    RISC-V base opcode map :cite:p:`riscv_isa`


Custom instructions in the RISC-V ISA
-------------------------------------
:numref:`tab-base` is a reminder base instructions format that can be found in the documentation.

.. _tab-base:
.. figure:: tables/base_formats.png
    :align: center

    RISC-V base instruction formats :cite:p:`riscv_isa`

Chapter 26 of the RISC-V specification :cite:p:`riscv_isa` includes several details about RISC-V extensions.

* According to :cite:p:`riscv_isa`, the base ISA is defined in a 30-bits space (bits 31-2 of the instruction).
* A prefix refer to the bits to the right of an encoding space. In this case, the prefix of the standard base ISA is only 2-bits wide and usually equal to :code:`11` (that is the reason why there is no :code:`inst[1:0]` in :numref:`tab-opcode`).

Example of the M extension
--------------------------

Here is the instructions set of the M extension:

.. _tab-mult:
.. figure:: tables/rv32m.png
    :align: center

    RV32M Standard Extension :cite:p:`riscv_isa`

This extension is composed of 8 instructions in the R format (4 for multiplication, 4 for division). The opcode :code:`0110011` is also used for some instructions of RVI :

* Add and sub between registers: :code:`add`, :code:`sub`
* "set if less than": :code:`slt`, :code:`sltu`
* Boolean arithmetics: :code:`xor`, :code:`or`, :code:`and`
* Shift-related operations: :code:`sll`, :code:`srl`, :code:`sra`

RVM differs from RVI mainly due to different :code:`funct7` values. It is also interesting to see that neither RVI nor RVM uses :code:`custom` opcodes shown in :numref:`tab-opcode` (conclusion should be the same for other ratified extensions): it confirms that it can be used for custom instructions.

Example of the cryptography extension
-------------------------------------

The study is based on the latest release available at the time of writing (Scalar Cryptograph :code:`v1.0.0-rc6`, chapter 5 may be interesting for constant-time related projects).
There are several extensions in this specification:

* :code:`Zbkb` for bit manipulations
* :code:`Zbkc` for carry-less multiply
* :code:`Zbkx` for crossbar permutation
* :code:`Zknd` for AES decryption
* :code:`Zkne` for AES encryption
* :code:`Zknh` for hash functions
* :code:`Zksed` for SM4 block cipher
* :code:`Zksh` for SM3 hash function
* :code:`Zkr` for entropy-related operations

A subset of instructions is shown in :numref:`t_crypto_subset`.

.. _t_crypto_subset:
.. figure:: tables/crypto.png
    :align: center

    Crypto extension subset :cite:p:`riscv_crypto`

In this case, two opcodes are used (:code:`0110011` and :code:`0010011`):

* The first is similar to the M extension.
* The second one is used for others RVI instructions such as :code:`addi`, :code:`xori`, :code:`slti` and so on.

As for the M extension, it mainly relies on :code:`funct7` and :code:`funct3` to create new instructions.